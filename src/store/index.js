import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        whiteLED         : 0,
        blueLED          : 0,
        greenLED         : 0,
        redLED           : 0,
        waterTemperature : 0,
        pumpLevel        : 0,
    },
    mutations: {
        'SYNC_WHITE_LED' (state, level) {
            state.whiteLED = level;
        },
        'SYNC_BLUE_LED' (state, level) {
            state.blueLED = level;
        },
        'SYNC_GREEN_LED' (state, level) {
            state.greenLED = level;
        },
        'SYNC_RED_LED' (state, level) {
            state.redLED = level;
        },
        'SYNC_WATER_TEMPERATURE' (state, level) {
            state.waterTemperature = level;
        },
        'SYNC_PUMP_LEVEL' (state, level) {
            state.pumpLevel = level;
        },

        'SET_WHITE_LED' (state, level) {
            state.whiteLED = level;
        },
        'SET_BLUE_LED' (state, level) {
            state.blueLED = level;
        },
        'SET_GREEN_LED' (state, level) {
            state.greenLED = level;
        },
        'SET_RED_LED' (state, level) {
            state.redLED = level;
        },
        'SET_PUMP_LEVEL' (state, level) {
            state.pumpLevel = level;
        },
        'SET_HEATER_LEVEL' (state, level) {
            state.waterTemperature = level;
        }
    },
    actions: {
        async syncWhiteLED({commit}) {
            try {
                const response = await Vue.axios.get('/get_led_info?ledName=white');
                commit('SYNC_WHITE_LED', response.data['level']/10);
            }
            catch(error) {
                console.error(error);
            }
        },
        async syncBlueLED({commit}) {
            try {
                const response = await Vue.axios.get('/get_led_info?ledName=blue');
                commit('SYNC_BLUE_LED', response.data['level']/10);
            }
            catch(error) {
                console.error(error);
            }
        },
        async syncGreenLED({commit}) {
            try {
                const response = await Vue.axios.get('/get_led_info?ledName=green');
                commit('SYNC_GREEN_LED', response.data['level']/10);
            }
            catch(error) {
                console.error(error);
            }
        },
        async syncRedLED({commit}) {
            try {
                const response = await Vue.axios.get('/get_led_info?ledName=red');
                commit('SYNC_RED_LED', response.data['level']/10);
            }
            catch(error) {
                console.error(error);
            }
        },
        async syncWaterTemperature({commit}) {
            try {
                const response = await Vue.axios.get('/get_water_tmp');
                commit('SYNC_WATER_TEMPERATURE', response.data['water_temperature_c']);
            }
            catch(error) {
                console.error(error);
            }
        },
        async syncPumpLevel({commit}) {
            try {
                const response = await Vue.axios.get('/pump_level');
                commit('SYNC_PUMP_LEVEL', response.message);
            }
            catch(error) {
                console.error(error);
            }
        },


        async setWhiteLED({commit}, level) {
            try {
                await Vue.axios.get(`/led_controller?impulse=4000&level=${level*10}&ledName=white`);
                commit('SET_WHITE_LED', level);
            }
            catch(error) {
                console.error(error);
            }
        },
        async setBlueLED({commit}, level) {
            try {
                await Vue.axios.get(`/led_controller?impulse=4000&level=${level*10}&ledName=blue`);
                commit('SET_BLUE_LED', level);
            }
            catch(error) {
                console.error(error);
            }
        },
        async setGreenLED({commit}, level) {
            try {
                await Vue.axios.get(`/led_controller?impulse=4000&level=${level*10}&ledName=green`);
                commit('SET_GREEN_LED', level);
            }
            catch(error) {
                console.error(error);
            }
        },
        async setRedLED({commit}, level) {
            try {
                await Vue.axios.get(`/led_controller?impulse=4000&level=${level*10}&ledName=red`);
                commit('SET_RED_LED', level);
            }
            catch(error) {
                console.error(error);
            }
        },
        async setPumpLevel({commit}, level) {
            try {
                await Vue.axios.get(`/pump_control?params=${level}`);
                commit('SET_PUMP_LEVEL', level);
            }
            catch(error) {
                console.error(error);
            }
        },
        async setWaterTemperature({commit}, level) {
            try {
                await Vue.axios.get(`/heater_control?params=${level}`);
                commit('SET_HEATER_LEVEL', level);
            }
            catch(error) {
                console.error(error);
            }
        },
    },
    getters: {
        whiteLED: state => {
            return state.whiteLED;
        },
        blueLED: state => {
          return state.blueLED;
        },
        greenLED: state => {
            return state.greenLED;
          },
        redLED: state => {
          return state.redLED;
        },
        waterTemperature: state => {
          return state.waterTemperature;
        },
        pumpLevel: state => {
          return state.pumpLevel;
        },
    }
})
